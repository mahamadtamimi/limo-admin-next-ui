import {Navbar} from "@/components/navbar";
import {Link} from "@nextui-org/link";
import {Head} from "./head";
import AdminSidebar from "@/components/Global/AdminSidebar";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
export default function AdminLayout({
                                          children,
                                      }: {
    children: React.ReactNode;
}) {
    return (
        <div className="relative flex flex-col h-screen">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"

            />
            {/* Same as */}
            <ToastContainer />
            <Head/>
            <Navbar/>
            <main className="w-full mx-auto  flex-grow px-3 py-3">
                <div className="flex gap-5">
                    <div className={'basis-1/6'}>
                        <AdminSidebar/>
                    </div>
                    <div className={'basis-5/6'}>
                        {children}
                    </div>
                </div>

            </main>

        </div>
    );
}
