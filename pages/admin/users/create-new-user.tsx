import AdminLayout from "@/layouts/admin";
import {Button, Input, Select, SelectItem} from "@nextui-org/react";
import {FormEvent} from "react";
import {toast} from "react-toastify";
import {useRouter} from "next/router";


export default function CreateNewUser() {

    const router = useRouter();

    function handleSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault()

        // @ts-ignore
        const formData = new FormData(e.target)

        // @ts-ignore
        if (formData.get('name').length === 0) {
            toast.error('نام کاربر الزامی است')
            return

        }
        // @ts-ignore
        if (formData.get('email').length === 0) {
            toast.error('ایمیل الزامی است')
            return

        }
        // @ts-ignore
        if (formData.get('phone').length === 0) {
            toast.error('شماره تماس الزامی است')
            return

        }

        // @ts-ignore
        if (formData.get('password').length === 0) {
            toast.error('پسورد الزامی است')
            return

        }

        toast.loading('لطفا صبر کنید!',
            {toastId: 'loadMsg'}
        )
        const data = {
            name: formData.get('name'),
            email: formData.get('email'),
            phone: formData.get('phone'),
            password: formData.get('password'),
        }


        fetch(`${process.env.API_PATH}/api/v1/users/create-new-user`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        }).then(res => res.json()).then(data => {
            toast.dismiss('loadMsg')
            if (data.success) {
                router.push('/admin/users/list')
            } else {
                toast.error(data.error)
            }


            console.log(data)
        })
    }

    return <AdminLayout>
        <div className={'bg-content1 rounded-large p-4'}>
            <div>
                افزودن کاربر جدید
            </div>


            <div className={'mt-5'}>
                <form action="" onSubmit={(e) => handleSubmit(e)}>
                    <div className={'flex gap-4'}>
                        <Input label={'نام'} className={'basis-1/3'} name={'name'}/>

                        <Input label={'ایمیل'} className={'basis-1/3'} name={'email'}/>
                        <Input label={'شماره تماس'} className={'basis-1/3'} name={'phone'}/>
                    </div>
                    <div className={'flex gap-4 mt-2'}>

                        <Input name={'password'} label={'پسورد'} className={'basis-1/2'}/>

                    </div>

                    <div className={'flex gap-4 mt-2'}>
                        <Select
                            name={'role'}
                            defaultSelectedKeys={["admin"]}
                            label="رول"
                            className='basis-1/2'
                        >
                            <SelectItem key={'admin'} value={'admin'}>
                                ادمین
                            </SelectItem>
                            <SelectItem key={'user'}>
                                کاربر
                            </SelectItem>
                        </Select>
                    </div>
                    <div className={'mt-4'}>
                        <Button type={'submit'}>
                            دخیره
                        </Button>
                    </div>
                </form>


            </div>

        </div>

    </AdminLayout>

}