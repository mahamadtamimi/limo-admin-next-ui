import AdminLayout from "@/layouts/admin";
import {
    Button,
    Input,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Select,
    SelectItem,
    useDisclosure
} from "@nextui-org/react";
import {Link} from "@nextui-org/link";
import {Textarea} from "@nextui-org/input";
import {ModalContent} from "@nextui-org/modal";
import {Simulate} from "react-dom/test-utils";
import submit = Simulate.submit;
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import change = Simulate.change;

export default function index() {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [inLoad, setInLoad] = useState(true)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState()

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState({
        source: {
            value: ''
        },
        size: {
            value: '',
        },
        quality: {
            value: '',
        }
    })


    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        if (router.query.id) {
            fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/source/${router.query.id}/show`, {})
                .then((res) => res.json())
                .then(data => {
                    setData(data.data)
                    setInLoad(false)

                    setFormData({
                        ...formData,
                        source: {
                            ...formData.source,
                            value: data.data.src
                        },
                        size: {
                            ...formData.size,
                            value: data.data.size
                        },
                        quality: {
                            ...formData.quality,
                            value: data.data.quality
                        }
                    })
                })
        }

    }, [router]);

    console.log(formData)

    function changeHandle(e : any) {
        e.preventDefault()
        switch (e.target.getAttribute("data-role")) {
            case "src":
                setFormData({
                    ...formData,
                    source:{
                        value: e.target.value
                    }
                })

                break;
            case "size":
                setFormData({
                    ...formData,
                    size: {
                        value: e.target.value
                    }
                })

                break;


        }
    }


    function hundleSubmit(e :any) {
        e.preventDefault()

        const formData = new FormData(e.target)

        const data = {
            'id' : router.query.id,
            'source': formData.get('source'),
            'size': formData.get('size'),
            'quality': formData.get('quality'),
        }

        fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/source/${router.query.id}/edit`, {
            method: "POST",
            headers :{
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        }).then(res => res.json()).then((data) => {
            router.back()
        })

    }


    return <AdminLayout>


        <div className={'bg-content1 rounded-large p-4 mb-4'}>

            <div>
                کیفیت


                <form action="" onSubmit={(e) => hundleSubmit(e)}>

                    <div>
                        {!inLoad &&
                            <Select
                                label="کیفیت"
                                className="w-full mt-4"
                                name="quality"
                                // @ts-ignore
                                defaultSelectedKeys={[(data.quality)]}
                            >

                                <SelectItem key={"480"} value={"480"}>

                                    480 P
                                </SelectItem>
                                <SelectItem key={'720'} value={'720'}>
                                    720 P
                                </SelectItem>

                            </Select>


                        }


                        <Input data-role={'size'} label={'حجم'} name={'size'} className={'mt-4'}
                               value={formData.size.value}
                               onChange={(e) => changeHandle(e)}/>
                        <Input data-role={'src'} label={'سورس'} name={'source'} className={'mt-4'}
                               value={formData.source.value}
                               onChange={(e) => changeHandle(e)}/>


                        <Button type={'submit'} className={'mt-4'}>
                            بروز رسانی
                        </Button>
                    </div>


                </form>


            </div>


        </div>
    </AdminLayout>


}