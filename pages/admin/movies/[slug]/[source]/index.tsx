import AdminLayout from "@/layouts/admin";
import {
    Button,
    Input,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Select,
    SelectItem,
    useDisclosure
} from "@nextui-org/react";
import {Link} from "@nextui-org/link";
import {Textarea} from "@nextui-org/input";
import {ModalContent} from "@nextui-org/modal";
import {Simulate} from "react-dom/test-utils";
import submit = Simulate.submit;
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {toast} from "react-toastify";

export default function index() {


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const {isOpen, onOpen, onOpenChange} = useDisclosure();

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [mainLangSource, setMainLangSource] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [subtitleSource, setSubtitleSource] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [dubbedSource, setDubbedSource] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [inLoad, setInLoad] = useState(true)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [lang, setLang] = useState()

    function submitSource(e : any) {
        // @ts-ignore
        toast.loading( 'لطفا صبر کنید !', {id:'loadMsg'})
        e.preventDefault()
        const formData = new FormData(e.target)

        const data = {
            'quality': formData.get('quality'),
            'size': formData.get('size'),
            'source': formData.get('source'),
            'lanq': formData.get('lanq'),
            'slug': router.query.slug
        }

        fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/source/add`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        }).then(res => res.json()).then((data) => {
            toast.dismiss('loadMsg')
            if (data.success) {
                router.reload()
            }else {
                toast.error('خطایی رخ داده است!')
            }
        })

    }

    // @ts-ignore
    const handleOpen = (langCode) => {
        setLang(langCode)
        onOpen();
    }
    // @ts-ignore
    const Module = ({langCode}) => (
        <>
            <Button onPress={() => handleOpen(langCode)} >افزودن سورس</Button>
            <Modal isOpen={isOpen} onOpenChange={onOpenChange}>
                <ModalContent>
                    {(onClose) => (
                        <>
                            <ModalHeader className="flex flex-col gap-1">افزودن سورس</ModalHeader>
                            <form action="" onSubmit={(e) => submitSource(e)}>
                                <ModalBody>

                                    <Select
                                        label="کیفیت"
                                        className="w-full mb-2"
                                        name="quality"
                                    >

                                        <SelectItem key={'480'} value={''}>
                                            480 P
                                        </SelectItem>
                                        <SelectItem key={'720'} value={''}>
                                            720 P
                                        </SelectItem>

                                    </Select>

                                 
                                    <Input label={'حجم'} name={'size'}/>
                                    <Input label={'سورس'} name={'source'}/>
                                    <input type={'hidden'} name={'lanq'} value={lang}/>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="danger" variant="light" onPress={onClose}>
                                        بستن
                                    </Button>
                                    <Button color="primary" type={'submit'}>
                                        افزودن
                                    </Button>
                                </ModalFooter>
                            </form>
                        </>
                    )}
                </ModalContent>
            </Modal>
        </>
    )





    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        if (router.query.slug) {
            fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/source/${router.query.slug}/list`)
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if (data.success) {
                        setMainLangSource(data.data.filter((item: any) => parseInt(item.lang) === 1))
                        setSubtitleSource(data.data.filter((item: any) => parseInt(item.lang) ===  2))
                        setDubbedSource(data.data.filter((item: any) => parseInt(item.lang) ===  3))

                        setInLoad(false)
                    }


                })


        }
    }, [router])


    function deleteSource(id : any) {
        console.log(id)
        fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/source/${id}/delete`)
            .then(res => res.json())
            .then(data => console.log(data))
    }


    console.log(mainLangSource)
    console.log(subtitleSource)

    // @ts-ignore
    return <AdminLayout>
        <div className={'bg-content1 rounded-large p-4 mb-4'}>

            <Button>
                اطلاعات کلی
            </Button>
            <Button as={Link}
                // href={`/admin/movies/${data.slug}/source`}
            >
                سورس ها
            </Button>
        </div>
        <div className={'rounded-large p-4'}>
            <div className={'flex gap-4'}>

                <div className={'basis-1/3 bg-content1 rounded-large p-4'}>
                    <div className={'justify-between flex align-middle'}>
                         <span className={'align-middle flex flex-col justify-around'}>
                                زبان اصلی
                          </span>

                        <Module langCode={1}/>

                    </div>
                    <div>

                        {!inLoad &&
                            // @ts-ignore
                            mainLangSource.length > 0 &&
                            // @ts-ignore
                            mainLangSource.map((item) => {
                                const id = item.id
                                return <div key={`main-lang-${id}`}
                                            className={'mt-2 bg-content2 rounded-large p-4'}>
                                    <div className={'flex justify-between mb-4'}>
                                        <div>
                                            <span>
                                                کیفیت :
                                            </span>
                                            <span>
                                            {item.quality}
                                        </span>
                                        </div>
                                        <div>
                                            <span>
                                                حجم :
                                            </span>
                                            <span>
                                            {item.size}
                                            </span>
                                        </div>

                                    </div>

                                    <p>{item.src}</p>
                                    <div className={'flex justify-end gap-2 mt-4'}>
                                        <span>
                                            <Button as={Link} href={`/admin/movies/${router.query.slug}/source/${item.id}/edit`}>
                                                ویرایش
                                            </Button>
                                        </span>
                                        <span>
                                             <Button onClick={() => deleteSource(id)}>
                                                خذف
                                            </Button>

                                        </span>

                                    </div>
                                </div>
                            })
                        }


                    </div>

                </div>


                <div className={'basis-1/3 bg-content1 rounded-large p-4'}>
                    <div className={'justify-between flex align-middle'}>
                         <span className={'align-middle flex flex-col justify-around'}>
                                زیر نویس
                          </span>

                        <Module langCode={2}/>

                    </div>
                    <div>

                        {!inLoad &&
                            // @ts-ignore
                            subtitleSource.length > 0 &&
                            // @ts-ignore
                            subtitleSource.map((item) => {
                                const id = item.id
                                return <div key={`sub-${id}`}
                                            className={'mt-2 bg-content2 rounded-large p-4'}>
                                    <div className={'flex justify-between mb-4'}>
                                        <div>
                                            <span>
                                                کیفیت :
                                            </span>
                                            <span>
                                            {item.quality}
                                        </span>
                                        </div>
                                        <div>
                                            <span>
                                                حجم :
                                            </span>
                                            <span>
                                            {item.size}
                                            </span>
                                        </div>

                                    </div>

                                    <p>{item.src}</p>
                                    <div className={'flex justify-end gap-2 mt-4'}>
                                        <span>
                                            <Button as={Link} href={`/admin/movies/${router.query.slug}/source/${item.id}/edit`}>
                                                ویرایش
                                            </Button>
                                        </span>
                                        <span>
                                             <Button onClick={() => deleteSource(id)}>
                                                خذف
                                            </Button>

                                        </span>

                                    </div>
                                </div>
                            })
                        }


                    </div>
                </div>

                <div className={'basis-1/3 bg-content1 rounded-large p-4'}>
                    <div className={'justify-between flex align-middle'}>
                         <span className={'align-middle flex flex-col justify-around'}>
                                دوبله
                          </span>

                        <Module langCode={3}/>

                    </div>
                    <div>

                        {!inLoad &&
                            // @ts-ignore
                            dubbedSource.length > 0 &&
                            // @ts-ignore
                            dubbedSource.map((item) => {
                                const id = item.id
                                return <div key={`dubbed-${id}`}
                                            className={'mt-2 bg-content2 rounded-large p-4'}>
                                    <div className={'flex justify-between mb-4'}>
                                        <div>
                                            <span>
                                                کیفیت :
                                            </span>
                                            <span>
                                            {item.quality}
                                        </span>
                                        </div>
                                        <div>
                                            <span>
                                                حجم :
                                            </span>
                                            <span>
                                            {item.size}
                                            </span>
                                        </div>

                                    </div>

                                    <p>{item.src}</p>
                                    <div className={'flex justify-end gap-2 mt-4'}>
                                        <span>
                                            <Button as={Link} href={`/admin/movies/${router.query.slug}/source/${item.id}/edit`}>
                                                ویرایش
                                            </Button>
                                        </span>
                                        <span>
                                             <Button onClick={() => deleteSource(id)}>
                                                خذف
                                            </Button>

                                        </span>

                                    </div>
                                </div>
                            })
                        }


                    </div>
                </div>

            </div>


            <div className={'mt-5'}>
                {/*<form action="" onSubmit={(e) => handleSubmit(e)}>*/}

                {/*</form>*/}


            </div>


        </div>

    </AdminLayout>


}