import AdminLayout from "@/layouts/admin";
import {Button, Input, Select, SelectItem} from "@nextui-org/react";
import {FormEvent, useEffect, useState} from "react";
import {toast} from "react-toastify";
import {useRouter} from "next/router";
import {Code} from "@nextui-org/code";
import 'react-image-upload/dist/index.css'
import ImageUploader from "react-image-upload";
import axios from "axios";
import {Textarea} from "@nextui-org/input";
import {Link} from "@nextui-org/link";

export default function CreateNewUser() {

    const router = useRouter();
    const [data, setData] = useState()
    const [inLoad, setInLoad] = useState(true)

    console.log(router.query.slug)

    useEffect(() => {
        if (router.query.slug) {
            fetch(`${process.env.MOVIES_API_PATH}/api/v1/media/${router.query.slug}`)
                .then(res => res.json())
                .then(data => {
                    setInLoad(false)
                    setData(data.data)
                    console.log(data)
                })
        }

    }, [router])


    function handleSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault()

        // @ts-ignore
        const formData = new FormData(e.target)

        const myHeaders = new Headers();

        // const formdata = new FormData();


        toast.loading('لطفا صبر کنید!',
            {toastId: 'loadMsg'}
        )
        const data = {
            name: formData.get('name'),
            englishName: formData.get('englishName'),
            originalName: formData.get('originalName'),
            slug: formData.get('slug'),
            label: formData.get('label'),
            imdb_rate: formData.get('imdb_rate'),
            limo_rate: formData.get('limo_rate'),
            imdb_sku: formData.get('imdb_sku'),
            age_certification: formData.get('age_certification'),
            duration: formData.get('duration'),
            release_date: formData.get('release_date'),
            cover: formData.get('main_cover'),
            wideCover: formData.get('wide_cover'),


        }

        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: formData,
            redirect: "follow"
        };

        // @ts-ignore
        fetch(`${process.env.MOVIES_API_PATH}/api/v1/movie/create`, requestOptions)
            .then(res => res.json())
            .then(data => {
                toast.dismiss('loadMsg')
                if (data.success) {
                    // router.push('/admin/movies/list')
                } else {
                    // toast.error(data.error)
                }


                console.log(data)
            })
    }


    function handleImage(e : any) {
        let images = e.target.files[0]

        const myHeaders = new Headers();

        const formdata = new FormData();
        formdata.append("images", images, "NEwGiZ.jpg");

        const requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: formdata,
            redirect: "follow"
        };

        // @ts-ignore
        fetch("http://127.0.0.1:8002/api/v1/movie/create", requestOptions)
            .then((response) => response.text())
            .then((result) => {
                // second_cover
                console.log(result)


            })
            .catch((error) => console.error(error));


    }



    return <AdminLayout>
        {
            !inLoad &&
            <>
                <div className={'bg-content1 rounded-large p-4 mb-4'}>

                    <Button>
                        اطلاعات کلی
                    </Button>
                    <Button as={Link}
                        // @ts-ignore
                            href={`/admin/movies/${data.slug}/source`}
                    >
                        سورس ها
                    </Button>
                </div>
                <div className={'bg-content1 rounded-large p-4'}>
                    <div>
                        افزودن فیلم جدید
                    </div>


                    <div className={'mt-5'}>
                        <form action="" onSubmit={(e) => handleSubmit(e)}>
                            <div className={'flex flex-wrap '}>
                                <div className={'p-2'}>
                                    <Input label={'نام'} className={'basis-1/3'} name={'name'}/>

                                </div>
                                <div className={'p-2'}>
                                    <Input label={'نام لاتین'} className={'basis-1/3'} name={'englishName'}/>
                                </div>
                                <div className={'p-2'}>

                                    <Input label={'نام اصلی'} className={'basis-1/3'} name={'originalName'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'پیوند یکتا'} className={'basis-1/3'} name={'slug'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'لیبل'} className={'basis-1/3'} name={'label'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'امتیاز IMDB'} type={"number"} className={'basis-1/3'}
                                           name={'imdb_rate'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'امتیاز limo'} type={"number"} className={'basis-1/3'}
                                           name={'limo_rate'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'کد IMDB'} lang={''} className={'basis-1/3'} name={'imdb_sku'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'رده سنی'} className={'basis-1/3'} name={'age_certification'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'تاریخ انتشار'} className={'basis-1/3'} name={'release_date'}/>
                                </div>
                                <div className={'p-2'}>
                                    <Input label={'زمان فیلم'} className={'basis-1/3'} name={'duration'}/>
                                </div>
                            </div>

                            <div className={'p-2'}>
                                <Textarea
                                    label="توضیخات"
                                    className="w-full"
                                />
                            </div>
                            <div className={'flex flex-wrap '}>
                                <div className={'p-2'}>
                                    <label htmlFor="">
                                <span>
                                کاور
                                </span>
                                        <input type="file" name={'main_cover'}/>
                                    </label>
                                </div>
                                <div className={'p-2'}>

                                    <label htmlFor="">
                                <span>
                                2 کاور
                                </span>
                                        <input type="file" name={'wide_cover'}/>
                                    </label>
                                </div>
                            </div>

                            <div className={'mt-4 text-end'}>
                                <Button type={'submit'}>
                                    دخیره
                                </Button>
                            </div>
                        </form>


                    </div>


                </div>
            </>

        }


    </AdminLayout>

}