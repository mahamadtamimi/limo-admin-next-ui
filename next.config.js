/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    IMAGE_DIRECTORY : 'http://127.0.0.1:8000/storage/',
    API_PATH : 'http://127.0.0.1:8002' ,
    MOVIES_API_PATH : 'http://127.0.0.1:8002'
  },
}

module.exports = nextConfig
