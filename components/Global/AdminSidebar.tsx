import React from "react";
import {Link} from "@nextui-org/link";
import {gray} from "next/dist/lib/picocolors";

export default function AdminSidebar() {


    return <div className={'bg-content1 rounded-large p-4'}>
        <ul>
            <li><Link href={'/admin/movies/list'}>فیلم ها</Link></li>
            <li><Link href={'/admin/serials'}>سریال ها</Link></li>
            <li><Link href={'/admin/slider'}>اسلایدر</Link></li>
            <li><Link>کاربران</Link></li>
            <li><Link>کاربران</Link></li>
            <li><Link>کاربران</Link></li>
            <li><Link>کاربران</Link></li>

        </ul>
    </div>
}